// Objets de superhéros
var Batman = {
    superHeroName: 'Batman',
    name: ['Bruce', 'Wayne'],
    age: 45,
    city: 'Gotham',
    ShowBio: function () {
        var node = document.createTextNode(this.superHeroName);
        document.getElementById('superHeroName').appendChild(node);

        var node = document.createTextNode("Le héro se nomme " + this.name[0] + " " + this.name[1] + ".");
        document.getElementById('name').appendChild(node);

        var node = document.createTextNode("Il est âgé de " + this.age + " ans.");
        document.getElementById('age').appendChild(node);

        var node = document.createTextNode("Et il vit à " + this.city + ".")
        document.getElementById('city').appendChild(node);
    },
};
    
var Catwoman = {
    superHeroName: 'Catwoman',
    name: ['Selina', 'Kyle'],
    age: 38,
    city: 'Gotham',
    ShowBio: function () {
        var node = document.createTextNode(this.superHeroName);
        document.getElementById('superHeroName').appendChild(node);

        var node = document.createTextNode("L\'héroine se nomme " + this.name[0] + " " + this.name[1] + ".");
        document.getElementById('name').appendChild(node);

        var node = document.createTextNode("Elle est âgée de " + this.age + " ans.");
        document.getElementById('age').appendChild(node);

        var node = document.createTextNode("Et elle vit à " + this.city + ".")
        document.getElementById('city').appendChild(node);
    },
};
    
var Spiderman = {
    superHeroName: 'Spider-Man',
    name: ['Peter', 'Parker'],
    age: 15,
    city: 'New York',
    ShowBio: function () {
        var node = document.createTextNode(this.superHeroName);
        document.getElementById('superHeroName').appendChild(node);

        var node = document.createTextNode("Le héro se nomme " + this.name[0] + " " + this.name[1] + ".");
        document.getElementById('name').appendChild(node);

        var node = document.createTextNode("Il est âgé de " + this.age + " ans.");
        document.getElementById('age').appendChild(node);

        var node = document.createTextNode("Et il vit à " + this.city + ".")
        document.getElementById('city').appendChild(node);
    },
};



let array = [Batman, Catwoman, Spiderman];
let index = prompt("Donnez un chiffre entre 1 et 3.");

index = giveIndexByPrompt(index);

array[index].ShowBio();

//Permet de convertir le chiffre donnée en index
function giveIndexByPrompt(param) {
    return param - 1;
}













